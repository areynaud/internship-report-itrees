# Report for my 2021 master internship. #

It was done at *ENS de Lyon*, CASH team, supervised by Ludovic Henrio and
Yannick Zakowsky. The goal was to implement a new bisimulation for interactions
trees parametrized by an equational theory.

**Requirements**
You need **minted** to compile the report.
